package testLista;



import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.ListaDoblementeEncadenada;
import model.vo.Service;
import model.vo.Taxi;

public class ListaTest 
{
	private ListaDoblementeEncadenada list;


	public void setupEscenario1()
	{
		list= new ListaDoblementeEncadenada<>();
	}
	public void setupEscenario2()
	{
		list= new ListaDoblementeEncadenada<>();
		Taxi elemento= new Taxi("abc","Compa�ia");
		list.add(elemento);
	}
	@Test
	public void addTest()
	{
		Taxi elemento= new Taxi("abc","Compa�ia");
		setupEscenario1();
		try
		{
			list.add(elemento);
			assertNotNull("Se deberia agregar el taxi",list.get(elemento));

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public void deleteTest()
	{
		setupEscenario2();
		try
		{
			list.delete(list.getByPos(0));
			assertEquals("El tama�o deberia ser 0",0,list.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public void getTest()
	{
		setupEscenario2();
		try
		{
			Taxi elemento= new Taxi("abf","comp");
			list.add(elemento);
			Taxi t= (Taxi) list.get(elemento);
			assertNotNull("No deberia ser nulo",t);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public void getByPos()
	{
		setupEscenario2();
		try
		{
			Taxi t=(Taxi) list.get(list.getByPos(0));
			assertNotNull("No deberia ser nulo",t);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
