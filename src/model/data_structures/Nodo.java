package model.data_structures;


public class Nodo<E extends Comparable<E>>
{
	//---------------------------------------------------------
	//ATRIBUTOS
	//--------------------------------------------------------------
	/**
	 * Elemento almacenado en el nodo.
	 */
	private E elemento;
	
	/**
	 * Siguiente nodo.
	 */
	private Nodo<E> siguiente;
	
	/**
	 * Nodo anterior.
	 */
	private Nodo<E> anterior;
	//------------------------------------------------------------------
	//CONSTRUCTOR
	//----------------------------------------------------------------
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenará en el nodo. elemento != null
	 */
	public Nodo(E elemento) 
	{
		this.elemento = elemento;

	}
	//---------------------------------------
	//Metodos
	//---------------------------------------
	public Nodo<E> darSiguiente()
	{
		return siguiente;
	}
	
	public void cambiarSiguiente(Nodo<E> siguiente)
	{
		this.siguiente = siguiente;
	}
	

	public E darElemento()
	{
		return elemento;
	}
	
	
	public void cambiarElemento(E elemento)
	{
		this.elemento = elemento;
	}
	
	public Nodo<E> darAnterior()
	{
		return anterior;
	}
	
	public void cambiarAnterior(Nodo<E> anterior)
	{
		this.anterior = anterior;
	}
}
