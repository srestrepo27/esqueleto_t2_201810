package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the first element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <E extends Comparable<E>> 
{
	
	public boolean add(E elemento);
	
	public boolean delete(E elemento);
	
	public E get(E elemento);
	
	public int size();
	
	public E getByPos(int pos);
	
	public E[] listing(E[] arreglo);
	
	public E getCurrent();
	
	public E set(int pos, E elemento);
	
}
