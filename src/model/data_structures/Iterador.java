package model.data_structures;

import java.util.ListIterator;



public class Iterador<E extends Comparable<E>> implements ListIterator<E> 
{


		//----------------------------------------------
		//Atributos
		//----------------------------------------------
		private Nodo<E> anterior;

		
		private Nodo<E> actual;
		//--------------------------------------------
		//Constructor
		//--------------------------------------------
		public Iterador(Nodo<E> primerNodo)
		{
			actual = primerNodo;
			anterior = null;
		}
		//-----------------------------------------------
		//Metodos
		//------------------------------------------------
		public boolean hasNext() 
		{
			return actual.darSiguiente()!=null;
		}

		
		public boolean hasPrevious() 
		{
			return anterior!=null;
		}

		public E getCurrent()
		{
			return  actual.darElemento();
		}
		public E next() 
		{
			E retornar=actual.darSiguiente().darElemento();
			anterior= actual;
			actual=(Nodo<E>) actual.darSiguiente();
			return retornar;
		}

		/**
		 * Devuelve el elemento anterior de la iteración y retrocede.
		 * @return elemento anterior de la iteración.
		 */
		public E previous() 
		{
			
			E retornar= actual.darAnterior().darElemento();
			actual=anterior;
			anterior=anterior.darAnterior();
			return retornar;
		}


		//=======================================================
		// Métodos que no se implementarán
		//=======================================================

		public int nextIndex() 
		{
			throw new UnsupportedOperationException();
		}

		public int previousIndex() 
		{
			throw new UnsupportedOperationException();
		}

		public void remove() 
		{
			throw new UnsupportedOperationException();
		}

		public void set(E e) 
		{
			throw new UnsupportedOperationException();
		}

		public void add(E e) 
		{
			throw new UnsupportedOperationException();		
		}
}

