package model.data_structures;

import java.util.Iterator;






public class ListaDoblementeEncadenada<E extends Comparable<E>> implements LinkedList<E>
{
	
	private Nodo<E> header;
	
	private Nodo<E> trailer;
	
	private Iterador<E> it= new Iterador<E>(header);
	
	private int cantidad=0;
	
	public ListaDoblementeEncadenada() 
	{
		trailer=null;
		header = null;
		cantidad= 0;
	}

	@Override
	public boolean add(E elemento)
	{
		// TODO Auto-generated method stub
		if(elemento == null)
		{
			throw new NullPointerException( );
		}
		
		Nodo<E> primerNodo= header;
		boolean agregado = false;
		
		if(primerNodo == null)
		{
			primerNodo = new Nodo<E>( elemento );
			agregado = true;
			cantidad++;
		}
		else
		{
			Nodo<E> actual = (Nodo<E>) primerNodo.darSiguiente();
			Nodo<E> anterior=(Nodo<E>) primerNodo;
			boolean existe = false;
			while(  actual.darSiguiente( ) != null && !existe&& anterior.darSiguiente()!=null)
			{
				if(actual.darElemento().compareTo(elemento)==0)
				{
					existe = true;
				}
				actual = (Nodo<E>) actual.darSiguiente( );
				anterior= (Nodo<E>) anterior.darSiguiente();
			}
			if(actual.darElemento().compareTo(elemento)!=0)
			{
				actual.cambiarAnterior( (Nodo<E>) new Nodo<E>( elemento ) );
				anterior.cambiarSiguiente( new Nodo<E>( elemento ));
				cantidad++;
			}

		}

		return agregado;
	}
		
	

	@Override
	public boolean delete(E elemento) 
	{
		// TODO Auto-generated method stub
		Nodo<E> primerNodo= header.darSiguiente();
		boolean elimino=false;
		Nodo<E> actual=(Nodo<E>) primerNodo.darSiguiente( );
		E buscado= (E)elemento;
		Nodo<E> anterior=header;
		if(buscado.compareTo(primerNodo.darElemento())==0)
		{
			primerNodo= actual.darSiguiente();
			((Nodo<E>) actual.darSiguiente()).cambiarAnterior(null);
			cantidad--;
		}
		while(elimino==false && actual!=null)
		{
			if(buscado.compareTo(actual.darElemento())==0)
			{
				anterior.cambiarSiguiente( actual.darSiguiente() );	
				((Nodo<E>) actual.darSiguiente()).cambiarAnterior(anterior);


				elimino=true;
				cantidad--;
			}
			else
			{
				anterior=actual;
				actual=(Nodo<E>) actual.darSiguiente( );
			}
		}
		return elimino;
	}

	@Override
	public E get(E elemento) 
	{
		Nodo<E> primerNodo= header.darSiguiente();
		Nodo<E> actual= primerNodo;
		E buscado= null;
		
		while(actual!=null)
		{
			
			if(elemento.compareTo(actual.darElemento())==0)
			{
				buscado=actual.darElemento( );
			}
			actual=actual.darSiguiente( );
		}
		return buscado;
	}

	@Override
	public int size() 
	{
		// TODO Auto-generated method stub
		return cantidad;
	}

	@Override
	public E getByPos(int pos)
	{
		Nodo<E> primerNodo= header.darSiguiente();
		Nodo<E> actual= primerNodo;
		E buscado= null;
		int contador=0;

		if(pos<0||pos>=size())
		{
			throw new IndexOutOfBoundsException("Pide " + pos + " y el tamanho es " + size());
		}

		while(actual!=null)
		{

			if(pos==contador)
			{
				buscado=actual.darElemento( );
			}
			actual=actual.darSiguiente( );
			contador++;
		}
		return buscado;
	}

	@Override
	public E[] listing(E[] array)
	{
		// TODO Auto-generated method stub
		Nodo<E> actual= header;
		int contador=0;
		if(actual!=null)
		{
			array[contador]=actual.darElemento();
			contador++;
		}
		while(actual.darSiguiente()!=null)
		{
			actual=actual.darSiguiente();
			array[contador]= actual.darElemento();
			contador++;
		}
		return array;
	}

	@Override
	public E getCurrent() 
	{
		// TODO Auto-generated method stub
		return it.getCurrent();
	}

	@Override
	public E set(int pos, E elemento)
	
	{
		Nodo<E> primerNodo= header.darSiguiente();
		Nodo<E> actual = primerNodo.darSiguiente(); 
		int contador=0;
		boolean elimine=false;

		if(pos<0|| pos>=size())
		{

			throw new IndexOutOfBoundsException();
		}

		while(actual!= null&& elimine==false)
		{
			if(contador==pos)
			{
				actual.cambiarElemento(elemento);
				elimine=true;
			}
			else
			{
				actual=actual.darSiguiente();
				contador++;
			}
		}
		return elemento;
	}
	public Iterador<E> darIterador()
	{
		return it;
	}

	


}
