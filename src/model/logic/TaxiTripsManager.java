package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.ListaDoblementeEncadenada;
import model.vo.Service;
import model.vo.Taxi;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;



public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private ListaDoblementeEncadenada<Service> lista;


	public void loadServices (String serviceFile)
	{
		JsonParser parser= new JsonParser();
		lista= new ListaDoblementeEncadenada<Service>();

		try
		{
			JsonArray listaGson= (JsonArray) parser.parse(new FileReader(serviceFile));

			for(int i=0; listaGson!=null && i< listaGson.size();i++)			
			{
				serviceFile = "./data/taxi-trips-wrvz-psew-subset-small.json";

				/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
				JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

				/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
				JsonObject obj= (JsonObject) arr.get(i);
				/* Mostrar un JsonObject (Servicio taxi) */
				//System.out.println("------------------------------------------------------------------------------------------------");
				//System.out.println(obj);
				/* Obtener la propiedad company de un servicio (String)*/
				String company = "N/A";
				if(obj.get("company") != null)
				{ 
					company = obj.get("company").getAsString();	
				}
				/* Obtener la propiedad community de un servicio (String) */
				short community = 0;
				if ( obj.get("community") != null )
				{ 
					community = obj.get("community").getAsShort();
				}

				/* Obtener la propiedad taxi_id de un servicio (String) */
				String taxi_id = "N/A";
				if ( obj.get("taxi_id") != null )
				{
					taxi_id = obj.get("taxi_id").getAsString(); 
				}


				/* Obtener la propiedad trip_id de un servicio (String) */
				String trip_id = "N/A";
				if ( obj.get("trip_id") != null )
				{ 
					trip_id = obj.get("trip_id").getAsString(); 
				}

				/* Obtener la propiedad miles de un servicio (int) */
				double miles = 0;
				if ( obj.get("miles") != null )
				{
					miles = obj.get("miles").getAsDouble();
				}

				/* Obtener la propiedad seconds de un servicio (int) */
				int seconds = 0;
				if ( obj.get("seconds") != null )
				{ 
					seconds = obj.get("seconds").getAsInt(); 
				}

				/* Obtener la propiedad total de un servicio (int) */
				double total = 0;
				if ( obj.get("miles") != null )
				{
					total = obj.get("miles").getAsDouble();
				}

				Taxi nuevoTaxi= new Taxi(company,taxi_id);
				Service nuevoServicio = new Service(trip_id,nuevoTaxi, seconds, miles, total,community);
				//Anadir el nuevo servicio a la lista
				lista.add(nuevoServicio);	


			}
		}
	

	catch (JsonIOException e1 ) 
	{
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	catch (JsonSyntaxException e2) 
	{
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	catch (FileNotFoundException e3) 
	{
		// TODO Auto-generated catch block
		e3.printStackTrace();	
	}

		System.out.println("Se cargaron exitosamente los datos");
	System.out.println("Inside loadServices with " + serviceFile);
}

@Override
public LinkedList<Taxi> getTaxisOfCompany(String company) {
	// TODO Auto-generated method stub
	ListaDoblementeEncadenada<Taxi> listaTaxis= new ListaDoblementeEncadenada<>();
	while(lista.getCurrent()!=null )
	{
		if(lista.getCurrent().getTaxi().getCompany().compareTo(company)==0 )
		{
			if(verificarNoRepetidos(lista.getCurrent().getTaxi(), listaTaxis)==false)
			{
				listaTaxis.add(lista.getCurrent().getTaxi());
			}
		}

		lista.darIterador().next();
	}
	System.out.println("Inside getTaxisOfCompany with " + company);
	return listaTaxis;
}

@Override
public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
	// TODO Auto-generated method stub
	ListaDoblementeEncadenada<Service> listaServicio= new ListaDoblementeEncadenada<>();
	while(lista.getCurrent()!=null )
	{
		if(lista.getCurrent().getCommunity()==communityArea)
		{
			listaServicio.add(lista.getCurrent());
		}

		lista.darIterador().next();
	}
	System.out.println("Inside getTaxisOfCompany with " + communityArea);
	return listaServicio;
}

public boolean verificarNoRepetidos(Taxi pTaxi, ListaDoblementeEncadenada<Taxi> pLista)
{
	boolean repetidos=false;
	for(int i=0; i<pLista.size(); i++)
	{
		if( pLista.getByPos(i).getTaxiId().compareTo(pTaxi.getTaxiId())==0)
		{
			repetidos=true;
		}
	}
	return repetidos;
}
}
