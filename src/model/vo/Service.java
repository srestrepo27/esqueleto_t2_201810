package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	//--------------------------------------------
	//Atributos
	//-----------------------------
	private String tripId;
	
	private Taxi taxi;
	
	private int seconds;
	
	private double miles;
	
	private double total;
	
	private short community;
	//-----------------------------
	//Constructor
	//-----------------------------
	public Service(String pId, Taxi pTaxi, int pSec, double pMiles, double pTotal, short pCommunity)
	{
		tripId=pId;
		taxi=pTaxi;
		seconds= pSec;
		miles=pMiles;
		total=pTotal;
		community=pCommunity;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi.getTaxiId(); 
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return total;
	}
	
	public Taxi getTaxi()
	{
		return taxi;
	}
	public short getCommunity()
	{
		return community;
	}
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
	return 0;
	}
}
